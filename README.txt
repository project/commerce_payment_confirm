Commerce Payment Confirm

Payment confirmation dialog for Drupal Commerce.

When this module is enabled, a confirmation dialog will pop up when a shopper 
hits the "pay now" button on checkout.

The dialog shows the total purchase amount and warns the users their 
credit card will be charged that amount if they proceed.

The dialog uses the jqueryUI dialog widget.

Built on request from a client, I thought it might be useful to the community.

The current code works but is pretty basic.

Note: This module is intended for on-site credit card payment methods, 
that is methods that involve the shopper entering their credit card details 
on the commerce web site (eg authorize.net) , rather than off-site methods 
that redirect the shopper to another site to complete the purchase (eg paypal).

Requires:
Drupal Commerce.

Installation:
Download the module to your usual contrib module directory, enable via the
modules admin screen.

Configuration:
The confirmation dialog displays a warning about the total amount of the order.
Below this is an admin configurable message. You can set this message by going
to admin/commerce/config/payment_confirm.
